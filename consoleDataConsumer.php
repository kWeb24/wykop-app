<?php
require_once __DIR__ . "/bootstrap.php";

use \Predis\Client;
use \Simpleue\Queue\RedisQueue;
use \Simpleue\Worker\QueueWorker;
use App\Jobs\PostUsersJob;

$redisQueue = new RedisQueue(new Client('127.0.0.1:6379'), 'users_queue');
$consumer = new QueueWorker($redisQueue, new PostUsersJob());
$consumer->start();
