<?php
	require_once __DIR__ . "/bootstrap.php";

	use App\Api\ApiConnector;
	$apiConnector = new ApiConnector();

	if (isset($_GET['page'])) {
		$getResult = $apiConnector->getUsers($_GET['page']);
	} else {
		$getResult = $apiConnector->getUsers();
	}

	if (isset($_POST['name']) && isset($_POST['job'])) {
		$newUser = $apiConnector->addUserToQueue($_POST['name'], $_POST['job']);
	}

	$queue = $apiConnector->getUserQueue();
?>

<!DOCTYPE html>
<html lang="pl">
	<head>
		<title>Kamil Weber</title>
		<meta charset="utf-8">
		<meta name="description" content="">
		<meta name="keywords" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<!--[if IE]>
		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->

		<style>
			.app-outer {
				width: 100vw;
				height: 100vh;
				position: relative;
			}

			.forms {
				position: absolute;
				top: 50%;
				left: 50%;
				transform: translate(-50%, -50%);
				padding: 30px;
			}

			.get-form, .post-form {
				max-width: 500px;
				width: 100%;
			}

			form {
				border: solid 1px rgb(128, 128, 128);
				padding: 15px;
				display: inline-block;
				width: 100%;
			}

			.row, label, input {
				width: 100%;
				float: left;
			}

			input {
				margin: 10px 0;
			}

			.preview {
					display: inline-block
			}

			.avatar {
				width: 50px;
				float: left;
			}

			.info {
				width: calc(100% - 50px);
				float: left;
			}
		</style>
	</head>
	<body>
		<div id="app-outer" class="app-outer">
			<div class="forms">
				<div class="get-form">
					<form action="#" method="get">
						<div class="row">
							<label for="page">Numer strony: </label>
							<input id="page" type="number" name="page" value="<?php echo $getResult['page']; ?>" />
						</div>
						<div class="row">
							<button type="submit">Pobierz</button>
						</div>
					</form>
					<div class="result">
						<p>
							Strona <span>
								<?php
									echo $getResult['page'] . '/' . $getResult['total'];
								?>
							</span>
							| Łącznie rekordów <span>
								<?php echo $getResult['users']; ?>
							</span> <?php if ($getResult['first']) echo ' | ' . $getResult[0]['cache']; ?> <?php if (!$getResult['first']) echo ' | Brak danych'; ?>
						</p>
						<div class="preview">
							<?php if ($getResult['first']) { ?>
							<div class="avatar">
								<img src="<?php echo $getResult['first']->avatar; ?>" alt="<?php echo $getResult['first']->first_name . ' ' . $getResult['first']->last_name; ?>" style="width: 100%;"/>
							</div>
							<div class="info">
								<p style="padding-left: 15px;">
									<?php echo $getResult['first']->id; ?> | <?php echo $getResult['first']->first_name . ' ' . $getResult['first']->last_name;; ?>
								</p>
							</div>
						<?php }?>
						</div>
					</div>
				</div>

				<div class="post-form">
					<form action="#" method="post">
						<?php if (isset($newUser) && $newUser) { ?>
							<div class="row">
								<p class="info">
									Użytkownik <?php echo 'ID: ' . $newUser['id'] . ' dodany!'; ?>
								</p>
							</div>
						<?php } ?>
						<div class="row">
							<label for="name">Imie</label>
							<input id="name" type="text" name="name" placeholder="Imie" required />
						</div>
						<div class="row">
							<label for="job">Praca</label>
							<input id="job" type="text" name="job" placeholder="Praca" required />
						</div>
						<div class="row">
							<button type="submit">Dodaj</button>
						</div>
					</form>
					<div class="preview">
						<p>
							Kolejka Redis (ostatnie 5):<br />
							<?php
								foreach ($queue as $key=>$user) {
									$item = json_decode($user);
									echo $key . '. ' . $item->name . ' ' . $item->job . '<br />';
								}
							?>
						</p>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>
