<?php

use PHPUnit\Framework\TestCase;
use App\Api\ApiConnector;
use App\Memcached\Memcached;
use \Predis\Client;

class ApiConnectorTest extends TestCase {
  public function testGetUsersReturningCacheArrayWhenCacheIsSet() {
      $stub = $this->getMockBuilder(ApiConnector::class)
                    ->setMethods(['isCached'])
                    ->getMock();

      $dummyDigit = 1;
      $dummyArray = ['dummy' => 'dummy'];
      $isCacheReturn = [
        'page' => $dummyDigit,
        'total' => $dummyDigit,
        'users' => $dummyDigit,
        'first' => $dummyArray
      ];

      $stub->expects($this->any())->method('isCached')->willReturn($isCacheReturn);
      $response = $stub->getUsers();
      $this->assertArrayHasKey('page', $response);
      $this->assertArrayHasKey('total', $response);
      $this->assertArrayHasKey('users', $response);
      $this->assertArrayHasKey('first', $response);
      $this->assertArrayHasKey('cache', $response[0]);
      $this->assertEquals($response[0]['cache'], "cached");
  }

  public function testIfUserIsAddedToQueueOnlyOnce() {
      $redisStub = $this->getMockBuilder(Client::class)
                      ->disableOriginalConstructor()
                      ->setMethods(['lpush'])
                      ->getMock();

      $redisStub->expects($this->once())->method('lpush')->willReturn(1);

      $stub = $this->getMockBuilder(ApiConnector::class)
                    ->setConstructorArgs([null, null, null, $redisStub])
                    ->setMethods()
                    ->getMock();

      $this->assertNull($stub->addUserToQueue('dumy', 'dummy'));
  }

  public function testCreateUserReturnsIdFromResponse() {
      $stub = $this->getMockBuilder(ApiConnector::class)
                    ->setMethods(['callApi'])
                    ->getMock();

      $dummyReturn = [
        "name" => "morpheus",
        "job"=> "leader",
        "id" => "823",
        "createdAt" => "2018-07-02T20:16:29.094Z"
      ];

      $stub->expects($this->any())->method('callApi')->willReturn($dummyReturn);
      $response = $stub->createUser('dummy', 'dummy');
      $this->assertArrayHasKey('id', $response);
  }
}
