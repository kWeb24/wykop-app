<?php

namespace App\Api;

use App\Memcached\Memcached;
use \Predis\Client;

class ApiConnector {
  private $apiBasePath;
  private $route;
  private $memcached;
  private $redis;

  public function __construct($apiBasePath = 'https://reqres.in',
                              $route = '/api/users',
                              Memcached $memcached = null,
                              Client $redis = null) {

    $this->apiBasePath = $apiBasePath;
    $this->route = $route;
    $this->memcached = ($memcached !== null) ? $memcached : new Memcached();
    $this->memcached->addServer("127.0.0.1", 11211);
    $this->redis = ($redis !== null) ? $redis : new Client('127.0.0.1:6379');
  }

  public function getUsers($page = 1) {
    $cachedValue = $this->isCached('page-' . $page);
    if ($cachedValue) {
      $result = $cachedValue;
      $result[] = ['cache' => 'cached'];
    } else {
      $resultJson = $this->callApi('GET', ['page' => $page]);

      $resultObject = json_decode($resultJson);
      $result = [
        'page' => $resultObject->page,
        'total' => $resultObject->total_pages,
        'users' => $resultObject->total,
        'first' => ($resultObject->data) ? $resultObject->data[0] : false
      ];
      if ($resultObject->data) {
        $this->setCache('page-' . $page, $result);
        $result[] = ['cache' => 'new cache created'];
      }
    }

    return $result;
  }

  public function addUserToQueue($name, $job) : void {
    $this->redis->lpush("users_queue", json_encode(['name' => $name, 'job' => $job]));
  }

  public function getUserQueue() : array {
    return $this->redis->lrange("users_queue", 0, 5);
  }

  public function createUser($name, $job) {
    $resultJson = $this->callApi('POST', ['name' => $name, 'job' => $job]);
    $resultObject = json_decode($resultJson);
    $result = [
      'id' => $resultObject->id
    ];
    return $result;
  }

  private function callApi($method, $data) {
    $curl = curl_init();

    switch($method) {
      case 'POST':
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        $url = $this->apiBasePath . $this->route;
        break;
      default:
        $url = sprintf("%s?%s", $this->apiBasePath . $this->route , http_build_query($data));
        break;
    }

    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl, CURLOPT_URL, $url);

    $resultJson = curl_exec($curl);

    if (!$resultJson) {
      die("Connection Failure");
    }

    curl_close($curl);
    return $resultJson;
  }

  private function isCached($key) {
    return $this->memcached->get($key);
  }

  private function setCache($key, $param, $compressed = 0, $ttl = 60) : void {
    $this->memcached->set($key, $param, $compressed, $ttl) or die ("Could not create keys");
  }
}
