<?php

namespace App\Jobs;

use \Simpleue\Job\Job;
use App\Api\ApiConnector;

class PostUsersJob implements Job {
  protected $apiConnector;
  public function __construct() {
    print("Job started...\n");
    $this->apiConnector = new ApiConnector();
  }

  public function manage($job) {
    print("Managing new job...\n");
    $jobObject = json_decode($job);
    print("createUser(" . $jobObject->name . ", " . $jobObject->job . ")\n");
    $result = $this->apiConnector->createUser($jobObject->name, $jobObject->job);
    print("User created: " . json_encode($result) . "\nWaiting for new job...\n");
    return $result;
  }

  public function isStopJob($job) {
    return false;
  }

  public function isMyJob($job) {
    return true;
  }
}
